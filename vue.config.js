module.exports = {
  runtimeCompiler: true,
  publicPath: process.env.NODE_ENV === 'production'
    ? '/gitlab-commit-tool/'
    : '/',
  transpileDependencies: [
    'vuetify',
  ],
}
