const BUILD = process.argv[2]

const EXPORT_CONTENT = `export default '${BUILD}'\n`

const fs = require('fs')
const setBuild = () => {
  fs.writeFile('src/build.js', EXPORT_CONTENT, 'utf-8', (err) => {
    if (err) throw err
    console.log(`
    BUILD_VERSION: ${BUILD}
    `)
  })
}

setBuild()
