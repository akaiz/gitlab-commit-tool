import Vue from 'vue'
import Vuex from 'vuex'

import projectService from '@/service/project'
import repoService from '@/service/repo'

import ids from './modules/ids'
import tokenModule from './modules/token'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    ids,
    tokenModule,
  },
  state: {
    project: {
      groupList: [],
      userList: [],
    },
  },
  mutations: {
    setProjectList (state, { type, data }) {
      state.project[`${type}List`] = data
    },
    setPorjectBranch (state, { project, data, tag }) {
      const index = state.project[`${tag}List`].findIndex(({ id }) => id === project)
      const cloneList = state.project[`${tag}List`].slice()
      cloneList[index].branches = data
      state.project[`${tag}List`] = cloneList
    },

  },
  actions: {
    async getProjectList ({ state, commit, dispatch }, options) {
      const type = typeof (options) === 'string' ? options : options.type
      switch (type) {
        case 'all':
          if (!state.project.groupList.length || options.isForce) await dispatch('getAllGroupProjects')
          if (!state.project.userList.length || options.isForce) await dispatch('getAllUserProjects')
          break
        case 'user':
          if (!state.project.userList.length || options.isForce) await dispatch('getAllUserProjects')
          break
        case 'group':
          if (!state.project.groupList.length || options.isForce) await dispatch('getAllGroupProjects')
      }
      return Promise.resolve()
    },
    async getAllGroupProjects ({ state, commit }) {
      const groupList = state.ids.id.group.map(({ id }) => id)
      const data = []
      for (const group of groupList) {
        await projectService.groups({ group })
          .then(res => {
            for (const { name, id, description } of res) {
              const resData = { name, id, description, tag: 'group' }
              if (!data.find(item => item.id === id)) data.push(resData)
            }
          })
          .catch(err => console.error(err))
      }
      commit('setProjectList', { type: 'group', data })
      return Promise.resolve()
    },

    async getAllUserProjects ({ state, commit }) {
      const userIDList = state.ids.id.user.map(({ id }) => id)
      const data = []
      for (const userID of userIDList) {
        await projectService.users({ userID })
          .then(res => {
            for (const { name, id, description } of res) {
              const resData = { name, id, description, tag: 'user' }
              if (!data.find(item => item.id === id)) data.push(resData)
            }
          })
          .catch(err => console.error(err))
      }
      commit('setProjectList', { type: 'user', data })
      return Promise.resolve()
    },
    async getProjectBranch ({ state, commit }, { project, tag, isForce }) {
      const projectItem = state.project[`${tag}List`].find(({ id }) => id === project)
      if (projectItem.branches && !isForce) return Promise.resolve()
      const data = []
      await repoService.branches({ project })
        .then(res => { for (const { name } of res) data.push(name) })
      data.sort((a, b) => {
        switch (a) {
          case 'master':
          case 'develop':
          case 'release':
            return -1
          default:
            return 1
        }
      })
      commit('setPorjectBranch', { project, data, tag })
      return Promise.resolve()
    },
  },
})
