import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  {
    path: '/:id?/:project?',
    redirect: '/instruction',
    component: () => import('@/layout/index.vue'),
    children: [
      {
        path: 'note',
        name: 'note',
        component: () => import('@/pages/Home.vue'),
      },
      {
        path: 'instruction',
        name: 'instruction',
        component: () => import('@/pages/instruction/index.vue'),
      },
      {
        path: '/cherry-pick/:type?/:project?/:branch?',
        name: 'cherry-pick',
        component: () => import('@/pages/cherry-pick'),
      },
      {
        path: '/setting/id-list',
        name: 'setting-ids',
        component: () => import('@/pages/settings/ids.vue'),
      },
      {
        path: '/setting/token',
        name: 'setting-token',
        component: () => import('@/pages/settings/token.vue'),
      },
    ],
  },

]

const router = new VueRouter({
  base: process.env.NODE_ENV === 'production'
    ? '/gitlab-commit-tool/'
    : '/',
  routes,
})

export default router
