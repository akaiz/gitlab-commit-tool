const Storage = {
  local: {
    set: (key, value) => {
      localStorage.setItem(key, JSON.stringify(value))
    },
    get: (key) => JSON.parse(localStorage.getItem(key)),
  },
  session: {
    set: (key, value) => {
      sessionStorage.setItem(key, JSON.stringify(value))
    },
    get: (key) => JSON.parse(sessionStorage.getItem(key)),
  },
}

export default Storage
