import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import vuetify from './plugins/vuetify'
import dayjs from 'dayjs'

import 'material-design-icons-iconfont/dist/material-design-icons.css'

import VueClipboard from 'vue-clipboard2'
import { mapActions } from 'vuex'

import Message from '@/components/message'

Vue.use(VueClipboard)
Vue.prototype.$dayjs = dayjs
Vue.prototype.$message = Message
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  created () {
    this.getIdList()
    this.getToken()
  },
  methods: {
    ...mapActions('ids', ['getIdList']),
    ...mapActions('tokenModule', ['getToken']),
  },
  render: h => h(App),
}).$mount('#app')
