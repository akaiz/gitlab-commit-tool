import Axios from 'axios'
import Store from '@/store'
import Message from '@/components/message'
const Service = ({ url = '', methods = 'get', isPrivateToken, data, params }) => {
  const service = Axios.create({
    baseURL: 'https://gitlab.com/api/v4',
    // timeout: 10000,
  })
  const options = { url, methods, data, params }
  if (isPrivateToken) {
    options.headers = {
      'PRIVATE-TOKEN': Store.state.tokenModule.active.token || 'TioBVZKnnfngcz_zkuRs',
    }
  }
  return new Promise((resolve, reject) => {
    service(options)
      .then(res => {
        resolve(res.data)
      }).catch(err => {
        if (err.response.status === 401) Message('private token 錯誤或已過期')
        reject(err)
      })
  })
}

export default Service
